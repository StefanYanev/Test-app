export class UserListModel{
    id: number = 0;
    first_name: string = '';
    last_name: string = '';
    username: string = '';
    email: string = '';
    password: string = '';
}