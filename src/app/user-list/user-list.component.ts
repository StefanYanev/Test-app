import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder  } from '@angular/forms';
import { UserListModel } from './user-list.model';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  formValue !:FormGroup;
  userObj: UserListModel = new UserListModel();
  userData: UserListModel[] = [];
  showAdd : any;
  showUpdate : any;
  page: number = 1;
  totalUsers: any;
  nameSort: any;
  searchType: any;

  constructor(private formBuilder: FormBuilder,
              private api: ApiService) { };

  ngOnInit(): void {
    this.formValue = this.formBuilder.group({
      first_name: [''],
      last_name: [''],
      username: [''],
      email: [''],
      password: ['']
    })

    this.getUserData();
  }

  postUserData(){
    this.userObj.first_name = this.formValue.value.first_name;
    this.userObj.last_name = this.formValue.value.last_name;
    this.userObj.username = this.formValue.value.username;
    this.userObj.email = this.formValue.value.email;
    this.userObj.password = this.formValue.value.password;

    this.api.postUser(this.userObj).subscribe(res=>{
      console.log(res);
      let ref = document.getElementById('cancel');
      ref?.click();
      this.formValue.reset();
      this.getUserData();
    })    
  }

  getUserData(){
    this.api.getUser().subscribe((res)=>{
      this.userData = res;
      this.totalUsers = res.length;
    })
  }

  deleteUser(row: any){
    this.api.deleteUser(row.id).subscribe(res=>{
      this.getUserData();
    });

  }

  clickAddUser(){
    this.formValue.reset();
    this.showAdd = true;
    this.showUpdate = false;
  }


  onEdit(row: any){
    this.showAdd = false;
    this.showUpdate = true;
    this.userObj.id = row.id;
    this.formValue.controls['first_name'].setValue(row.first_name);
    this.formValue.controls['last_name'].setValue(row.last_name);
    this.formValue.controls['username'].setValue(row.username);
    this.formValue.controls['email'].setValue(row.email);
    this.formValue.controls['password'].setValue(row.password);
  }

  editUser(){
    this.userObj.first_name = this.formValue.value.first_name;
    this.userObj.last_name = this.formValue.value.last_name;
    this.userObj.username = this.formValue.value.username;
    this.userObj.email = this.formValue.value.email;
    this.userObj.password = this.formValue.value.password;
    
    this.api.updateUser(this.userObj, this.userObj.id).subscribe(res=>{
      let ref = document.getElementById('cancel');
      ref?.click();
      this.formValue.reset();
      this.getUserData();
    })
  }

  Search(){
    if(this.nameSort == ""){
      this.ngOnInit();
    }else{
      if(this.searchType){
        if(this.searchType == "first_name"){
          this.userData = this.userData.filter(res =>{
            return res.first_name.toLocaleLowerCase().match(this.nameSort.toLocaleLowerCase());
          })
        }else if(this.searchType=="last_name"){
          this.userData = this.userData.filter(res =>{
            return res.last_name.toLocaleLowerCase().match(this.nameSort.toLocaleLowerCase());
          })
        }else if(this.searchType=="username"){
          this.userData = this.userData.filter(res =>{
            return res.username.toLocaleLowerCase().match(this.nameSort.toLocaleLowerCase());
          })
        }
      }else{
        this.userData = this.userData.filter(res =>{
          return res.first_name.toLocaleLowerCase().match(this.nameSort.toLocaleLowerCase());
        })
      }
    }
  }
}
