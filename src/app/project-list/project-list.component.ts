import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder  } from '@angular/forms';
import { ProjectListModel } from './project-list.model';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  formValue !:FormGroup;
  projectObj: ProjectListModel = new ProjectListModel();
  projectData: ProjectListModel[] = [];
  showAddBtn : any;
  showUpdateBtn : any;
  userData: any;
  pageProjects: Number = 2;
  page: number = 1;
  totalProjects: any;
  nameSort: any;
  

  constructor(private formBuilder: FormBuilder,
              private api: ApiService) { };

  ngOnInit(): void {
    this.formValue = this.formBuilder.group({
      p_name: [''],
      p_description: [''],
      p_start_date: [''],
      p_end_date: [''],
      p_category: [''],
      p_team: ['']
    })

    this.getProjectData();
    this.getUserData();
  }

  postProjectData(){
    this.projectObj.p_name = this.formValue.value.p_name;
    this.projectObj.p_description = this.formValue.value.p_description;
    this.projectObj.p_start_date = this.formValue.value.p_start_date;
    this.projectObj.p_end_date = this.formValue.value.p_end_date;
    this.projectObj.p_category = this.formValue.value.p_category;
    this.projectObj.p_team = this.formValue.value.p_team;

    this.api.postProject(this.projectObj).subscribe(res=>{
      console.log(res);
      let ref = document.getElementById('cancelProject');
      ref?.click();
      this.formValue.reset();
      this.getProjectData();
    })    
  }

  getProjectData(){
    this.api.getProject().subscribe((res)=>{
      this.projectData = res;
      this.totalProjects = res.length;
    })
  }

  getUserData(){
    this.api.getUser().subscribe(res=>{
      this.userData = res;
    })
  }

  deleteProject(row: any){
    this.api.deleteProject(row.id).subscribe(res=>{
      this.getProjectData();
    });

  }

  clickAddProject(){
    this.formValue.reset();
    this.showAddBtn = true;
    this.showUpdateBtn = false;
  }


  onEditProject(row: any){
    this.showAddBtn = false;
    this.showUpdateBtn = true;
    this.projectObj.id = row.id;
    this.formValue.controls['p_name'].setValue(row.p_name);
    this.formValue.controls['p_description'].setValue(row.p_description);
    this.formValue.controls['p_start_date'].setValue(row.p_start_date);
    this.formValue.controls['p_end_date'].setValue(row.p_end_date);
    this.formValue.controls['p_category'].setValue(row.p_category);
    this.formValue.controls['p_team'].setValue(row.p_team);
  }

  editProject(){
    this.projectObj.p_name = this.formValue.value.p_name;
    this.projectObj.p_description = this.formValue.value.p_description;
    this.projectObj.p_start_date = this.formValue.value.p_start_date;
    this.projectObj.p_end_date = this.formValue.value.p_end_date;
    this.projectObj.p_category = this.formValue.value.p_category;
    this.projectObj.p_team = this.formValue.value.p_team;
    
    this.api.updateProject(this.projectObj, this.projectObj.id).subscribe(res=>{
      let ref = document.getElementById('cancelProject');
      ref?.click();
      this.formValue.reset();
      this.getProjectData();
    })
  }

  Search(){
    if(this.nameSort == ""){
      this.ngOnInit();
    }else{
      this.projectData = this.projectData.filter(res =>{
        return res.p_name.toLocaleLowerCase().match(this.nameSort.toLocaleLowerCase());
      })
    }
  }
}
