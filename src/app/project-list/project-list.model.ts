export class ProjectListModel{
    id: number = 0;
    p_name: string = '';
    p_description: string = '';
    p_start_date: string = '';
    p_end_date: string = '';
    p_category: string = '';
    p_team: string= '';
}