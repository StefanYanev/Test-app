import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router"
import { ProjectListComponent } from "./project-list/project-list.component";
import { UserListComponent } from "./user-list/user-list.component";

const routes:Routes = [
    {path: 'users', component: UserListComponent},
    {path: 'projects', component: ProjectListComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule{}
export const routingComponents = [UserListComponent, ProjectListComponent]


